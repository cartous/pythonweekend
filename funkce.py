import json, requests

def fetch_data(link):
    fetch = requests.get(link)
    fetch_data = json.loads(fetch.text)
    return fetch_data