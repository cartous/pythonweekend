import argparse, sys
import json
import requests

#argument - defauklt hodnota

#Arguments + help
parser = argparse.ArgumentParser()
parser.add_argument("--cities", help="Cities with airports", action="store_true")
parser.add_argument("--coords", help="Coordinates of each airport", action="store_true")
parser.add_argument("--iata", help="IATA codes", action="store_true")
parser.add_argument("--names", help="Name of the airport", action="store_true")
parser.add_argument("--full", help="Print every detail from each airport", action="store_true")
args = parser.parse_args()

#default values = no argument was given
if len(sys.argv) == 1:
    args.names = args.iata = True
#specific arguments was given
elif args.full == True:
    #Question: Is there a better way how to set all arguments to True ? I.e. what if we would have 20 arguments ?
    args.cities = args.coords = args.iata = args.names = True

#URL for API
URL = "https://api.skypicker.com/locations?type=subentity&term=GB&locale=en-US&active_only=true&location_types=airport&limit=100&sort=name"


def fetch_data(link):
    fetch = requests.get(link)
    fetch_data = json.loads(fetch.text)
    return fetch_data

#check get

def parse_data(data, cities=args.cities, coords=args.coords, iata=args.iata, names=args.names):

    '''
    return {
        key: data.get(key) for key in ["cities", "coords"]
    }


    raw_data = data

    for key in ["cities", "coords"]:
        entry[key] = raw_data.get(key)
    '''

    #tohle jde nahradit obema variantama nahore
    #moznost vyuzit primo jmena argumentu nez True/False

    entry = {}
    raw_data = data

    if names == True:
        entry['name'] = raw_data['name']
    if iata == True:
        entry['iata'] = raw_data['id']
    if cities == True:
        entry['city'] = raw_data['city']['name']
    if coords == True:
        entry['coords'] = [raw_data['location']['lat'], raw_data['location']['lon']]
    return entry

def get_result(data):
    List = []
    for entry in data:
        List.append(parse_data(entry))
    return List

#Question: I could fetch data to variable and than send to parse_data function. (example: data = request.get(URL))
#If I send fetched data directly to function (like I did here), than it's not stored in memory after get_result function finish, right ?
#Since we are fetching few KB of data, it does not matter, but if it would be much more, would this be better way than storing in variable ?

print("Dictionary output:\n",get_result(fetch_data(URL)['locations']), "\n")


#1st way will return dictionary for easy access of items -> item['name'], etc.
#might be good if we dont know order of items

#############################################
#################Another Way#################

def parse_to_csv(data, cities=args.cities, coords=args.coords, iata=args.iata, names=args.names):
    entry = []
    raw_data = data
    if names == True:
        entry.append(raw_data['name'])
    if iata == True:
        entry.append(raw_data['id'])
    if cities == True:
        entry.append(raw_data['city']['name'])
    if coords == True:
        entry.append([raw_data['location']['lat'], raw_data['location']['lon']])
    return entry

def get_csv_header(cities=args.cities, coords=args.coords, iata=args.iata, names=args.names):
    header = []
    if names == True:
        header.append('name')
    if iata == True:
        header.append('iata')
    if cities == True:
        header.append("city")
    if coords == True:
        header.append('coords')
    return header

def get_csv_results(header, data):
    #using print for demonstration of output, but we could save it with "open file..."
    print(header)
    #limit number of result for DEMO
    limit = 10
    counter = 0
    for entry in data:
        if counter < 10:
            print(parse_to_csv(entry))
            counter += 1
        else:
            break

print("CSV output:")
get_csv_results(get_csv_header(), fetch_data(URL)['locations'])

